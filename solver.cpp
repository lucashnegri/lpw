#include "solver.h"
#include <sstream>
#include <algorithm>
#include <stdexcept>

extern "C"
{
#include <lpsolve/lp_lib.h>
}

namespace lpw
{

struct SolverPriv
{
    SolverPriv(lprec* ptr)
    {
        if(!ptr) // no allocations, no leak here
            throw std::runtime_error("could not create lprec* solver");

        lp = ptr;
        m_vars = get_Ncolumns(lp);
        colno.resize(m_vars);

        for(int i = 0; i < m_vars; ++i)
            colno[i] = i + 1;

        set_add_rowmode(lp, TRUE);
    }

    ~SolverPriv()
    {
        delete_lp(lp);
        lp = 0;
    }

    VI colno;
    lprec* lp;
    int m_vars;
};

Solver::Solver(int vars) : d(new SolverPriv(make_lp(0, vars)))
{
}

Solver::Solver(const std::string &path, const std::string &name) :
    d(new SolverPriv(read_LP((char*)path.c_str(), FALSE, (char*)name.c_str())))
{
}

Solver::~Solver()
{
    delete d;
}

VD Solver::makeRow()
{
    return VD(d->m_vars, 0);
}

void Solver::addConstraint(VD &lhs, ConstrType type, double rhs)
{
    addConstraint(lhs, d->colno, type, rhs);
}

void Solver::addConstraint(VD &lhs, VI& colno, ConstrType type, double rhs)
{
    if(lhs.size() != colno.size())
        throw std::runtime_error("size does not match");

    const int size = std::min(d->m_vars, static_cast<int>(lhs.size()));

    if(!add_constraintex(d->lp, size, &lhs[0], &colno[0], type, rhs))
        throw std::runtime_error("failed to add constraint");
}

void Solver::setObjective(VD &lhs, bool maximize)
{
    if(lhs.size() < d->m_vars)
        throw std::string("size does not match");

    const int size = std::min(d->m_vars, static_cast<int>(lhs.size()));
    set_obj_fnex(d->lp, size, &lhs[0], &d->colno[0]);

    if(maximize)
        set_maxim(d->lp);
    else
        set_minim(d->lp);
}

void Solver::setInt(int var, bool value)
{
    if(var <  0 || var >= d->m_vars)
        throw std::runtime_error("index out of bounds");
    
    set_int(d->lp, var+1, value);
}

void Solver::setSolution(int sol)
{
    if(sol <  0)
        throw std::runtime_error("index out of bounds");
    
    set_solutionlimit(d->lp, sol+1);
}

void Solver::setSimplexType(SimplexType type)
{
    set_simplextype(d->lp, static_cast<int>(type));
}

void Solver::setBinary(int var, bool value)
{
    if(var <  0 || var >= d->m_vars)
        throw std::runtime_error("index out of bounds");
    
    set_binary(d->lp, var+1, value);
}

void Solver::setBounds(int var, double lower, double upper)
{
    if(var <  0 || var >= d->m_vars)
        throw std::runtime_error("index out of bounds");
    
    set_bounds(d->lp, var+1, lower, upper);
}

void Solver::setTimeout(int timeout)
{
    set_timeout(d->lp, timeout);
}

int Solver::getElapsedTime()
{
    return time_elapsed(d->lp);
}

int Solver::getIterations()
{
    return get_total_iter(d->lp);
}

int Solver::getSolutions()
{
    return get_solutioncount(d->lp);
}

Result Solver::solve(VD &out, double& objective, int log)
{
    set_add_rowmode(d->lp, FALSE);
    set_verbose(d->lp, log);
    Result res = static_cast<Result>(::solve(d->lp));

    out.resize(d->m_vars);
    get_variables(d->lp, &out[0]);
    objective = get_objective(d->lp);

    return res;
}

void Solver::write(const std::string &path) const
{
    write_lp(d->lp, (char*)path.c_str());
}

}
