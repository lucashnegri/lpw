lpw
===

About
-----

lpw is a C++ wrapper for [lpsolve][1] 5.5, prividing a simple (but limited) API for linear programming.
All API is 0-based.

Dependencies
------------

Depends on [lpsolve][1].

License
-------

LGPLv3+.

Usage
-----

Usage examples are located on the *tests* folder. Basically, you must create a lpw::Solver, add the
constraints, set the objective value and call solve().

Compilation
-----------

    $ mkdir build 
    $ cd build
    $ cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local ..
    $ make -j
    # make install
    
To include lpw into your cmake project you can use the provided module located at
cmake/Findlpw.cmake.

-------

Lucas Hermann Negri - lucashnegri@gmail.com - [OProj][2]

[1]: http://sourceforge.net/projects/lpsolve
[2]: http://oproj.tuxfamily.org
