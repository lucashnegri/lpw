#ifndef LPW_SOLVER_H
#define LPW_SOLVER_H

#include <vector>
#include <string>
#include <cstdio>

namespace lpw
{

typedef std::vector<int>    VI;
typedef std::vector<double> VD;

enum ConstrType
{
    LE = 1,
    GE = 2,
    EQ = 3
};

enum Result
{
    OutOfMemory     = -2,
    Optimal         =  0,
    Suboptimal      =  1,
    Infeasible      =  2,
    Unbounded       =  3,
    Degenerate      =  4,
    NumFailure      =  5,
    UserAbort       =  6,
    Timeout         =  7,
    PresSolved      =  9,
    BBFail          = 10,
    BBBreak         = 11,
    FeasibleFound   = 12,
    NoFeasibleFound = 13
};

enum SimplexType
{
    SimplexPrimalPrimal = 5,
    SimplexDualPrimal   = 6,
    SimplexPrimalDual   = 9,
    SimplexDualDual     = 10
};

struct SolverPriv;

class Solver
{
public:
    Solver(int vars);
    Solver(const std::string& path, const std::string& name = "");
    virtual ~Solver();

    VD makeRow();

    // not const, due to lpsolve
    void addConstraint(VD &lhs, ConstrType type, double rhs);
    void addConstraint(VD &lhs, VI &colno, ConstrType type, double rhs);
    void setObjective(VD &lhs, bool maximize = true);

    void setInt(int var, bool value = true);
    void setSolution(int sol = 1);
    void setSimplexType(SimplexType type);
    void setBinary(int var, bool value = true);
    void setBounds(int var, double lower, double upper);
    void setTimeout(int timeout);
    int  getElapsedTime();
    int  getIterations();
    int  getSolutions();

    Result solve(VD& out, double& obj, int log = 1);
    void write(const std::string& path) const;

private:
    Solver(const Solver&);
    Solver& operator=(const Solver&);

    SolverPriv* d;
};

}
#endif // LPW_SOLVER_H
