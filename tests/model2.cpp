#include <iostream>
#include <vector>
#include "solver.h"

/*
 * min x + y + z
 *
 * subject to
 *   x + y + z >= 100
 *   x         >=  10
 *       y     >=  20
 *          2z >=  50
 */
lpw::Solver* loadOrCreateSolver()
{
    // try to load a saved model
    try
    {
        return new lpw::Solver("model.lp");
    }
    catch(...)
    {
        std::cout << "No valid model available, creating one" << std::endl;
    }

    try
    {
        lpw::Solver* solver = new lpw::Solver(3);
        lpw::VD row = solver->makeRow();

        row[0] = 1;
        row[1] = 1;
        row[2] = 1;
        solver->addConstraint(row, lpw::GE, 100);

        row[0] = 1;
        row[1] = 0;
        row[2] = 0;
        solver->addConstraint(row, lpw::GE, 10);

        row[0] = 0;
        row[1] = 1;
        row[2] = 0;
        solver->addConstraint(row, lpw::GE, 20);

        row[0] = 0;
        row[1] = 0;
        row[2] = 2;
        solver->addConstraint(row, lpw::GE, 50);

        row[0] = 1;
        row[1] = 1;
        row[2] = 1;
        solver->setObjective(row, false);

        solver->write("model.lp");
        return solver;
    }
    catch(...)
    {
        std::cout << "Could not create solver. Terminating" << std::endl;
        std::terminate();
    }

    return 0;
}

int main()
{
    lpw::Solver* solver = loadOrCreateSolver();

    try
    {
        double obj;
        lpw::VD row = solver->makeRow();
        lpw::Result res = solver->solve(row, obj);
        std::cout << "Objective: " << obj << std::endl;
        std::cout << "Result   : " << res << std::endl;
        std::cout << "x = " << row[0] << " y = " << row[1] << " z = " << row[2] << std:: endl;
    }
    catch(const std::string& exp)
    {
        std::cout << "Exception: " << exp << std::endl;
    }

    delete solver;

    return 0;
}


