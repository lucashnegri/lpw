#include <iostream>
#include <string>
#include "solver.h"

int main()
{
    try
    {
        while(true)
        {
            lpw::Solver solver(5);
            lpw::VD row = solver.makeRow();

            row[0] = 5;
            row[1] = 2;
            solver.addConstraint(row, lpw::GE, 100);

            row[0] = 1;
            row[2] = 1;
            solver.addConstraint(row, lpw::LE,  50);

            row[0] = 2;
            row[1] = 3;
            solver.setObjective(row, false);

            solver.setBounds(0, -200, 200);
            solver.setBounds(1, -200, 200);

            double obj;
            solver.solve(row, obj);
        }
    }
    catch(const std::string& e)
    {
        std::cout << "Exception: " << e << std::endl;
    }

    return 0;
}
