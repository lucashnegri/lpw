include_directories(${PROJECT_SOURCE_DIR})
link_directories(${PROJECT_SOURCE_DIR})

add_executable(model1 model1.cpp)
target_link_libraries(model1 lpw)

add_executable(model2 model2.cpp)
target_link_libraries(model2 lpw)

add_executable(model3 model3.cpp)
target_link_libraries(model3 lpw)

add_executable(memory memory.cpp)
target_link_libraries(memory lpw)
