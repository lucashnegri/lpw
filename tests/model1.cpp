#include <iostream>
#include <vector>
#include "solver.h"

/*
 * max 143 x + 60 y
 *
 * subject to
 *   120 x + 210 y <= 15000
 *   110 x + 30  y <= 4000
 *       x +     y <= 75
 */
int main()
{
    try
    {
        lpw::Solver solver(2);
        lpw::VD row = solver.makeRow();

        row[0] = 120;
        row[1] = 210;
        solver.addConstraint(row, lpw::LE, 15000);

        row[0] = 110;
        row[1] = 30;
        solver.addConstraint(row, lpw::LE, 4000);

        row[0] = 1;
        row[1] = 1;
        solver.addConstraint(row, lpw::LE, 75);

        row[0] = 143;
        row[1] = 60;
        solver.setObjective(row, true);

        double obj;
        lpw::Result res = solver.solve(row, obj);
        std::cout << "Objective: " << obj << std::endl;
        std::cout << "Result   : " << res << std::endl;
        std::cout << "x = " << row[0] << " y = " << row[1] << std:: endl;
    }
    catch(const std::string& exp)
    {
        std::cout << "Exception: " << exp << std::endl;
    }

    return 0;
}

